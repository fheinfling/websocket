//
//  JetfireWS.h
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <jetfire/JFWebSocket.h>

@interface JetfireWS : NSObject <JFWebSocketDelegate>

- (void)connectWebSocket;

@end
