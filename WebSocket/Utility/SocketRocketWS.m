//
//  SocketRocketWS.m
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import "SocketRocketWS.h"

@interface SocketRocketWS()
@property (nonatomic, strong) SRWebSocket *webSocket;
@end

@implementation SocketRocketWS

#pragma mark --- WebSocket ---

- (void)connectWebSocket
{
    _webSocket.delegate = nil;
    _webSocket = nil;
    
    NSString *webSocketURLString = @"ws://stark-bayou-3614.herokuapp.com";
    SRWebSocket *newSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:webSocketURLString]];
    newSocket.delegate = self;
    
    [newSocket open];
}


#pragma mark --- SRWebSocketDelegate ---

- (void)webSocketDidOpen:(SRWebSocket *)newWebSocket {
    _webSocket = newWebSocket;
//    [_webSocket send:[NSString stringWithFormat:@"Hello from %@", [UIDevice currentDevice].name]];
    NSLog(@"websocket did open");
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"message: %@", message);
}

@end
