//
//  JetfireWS.m
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import "JetfireWS.h"

@interface JetfireWS ()
@property (nonatomic, strong) JFWebSocket *webSocket;
@end

@implementation JetfireWS

#pragma mark --- WebSocket ---

- (void)connectWebSocket
{
    NSString *webSocketURLString = @"ws://stark-bayou-3614.herokuapp.com:80";
    _webSocket = [[JFWebSocket alloc] initWithURL:[NSURL URLWithString:webSocketURLString]];
    _webSocket.delegate = self;
    
    [_webSocket connect];
}

#pragma mark --- JFWebSocketDelegate ---

-(void)websocketDidConnect:(JFWebSocket*)socket
{
    NSLog(@"websocket is connected");
}


-(void)websocketDidDisconnect:(JFWebSocket*)socket error:(NSError*)error
{
    NSLog(@"websocket is disconnected: %@",[error localizedDescription]);
}


-(void)websocket:(JFWebSocket*)socket didReceiveMessage:(NSString*)string
{
    NSLog(@"got some text: %@",string);
    dispatch_async(dispatch_get_main_queue(),^{
        //do some UI work
    });
}


-(void)websocket:(JFWebSocket*)socket didReceiveData:(NSData*)data
{
    NSLog(@"got some binary data: %d",data.length);
}

@end
