//
//  PocketSocketWS.h
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PocketSocket/PSWebSocket.h>

@interface PocketSocketWS : NSObject <PSWebSocketDelegate>

- (void)connectWebSocket;

@end
