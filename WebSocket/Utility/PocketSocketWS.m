//
//  PocketSocketWS.m
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import "PocketSocketWS.h"

@interface PocketSocketWS ()
@property (nonatomic, strong) PSWebSocket *webSocket;
@end

@implementation PocketSocketWS

#pragma mark --- WebSocket ---

- (void)connectWebSocket
{
    NSString *webSocketURLString = @"ws://stark-bayou-3614.herokuapp.com:80";
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:webSocketURLString]];
    _webSocket = [PSWebSocket clientSocketWithRequest:request];
    _webSocket.delegate = self;

    [_webSocket open];
}


#pragma mark --- PSWebSocketDelegate ---

- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"The websocket handshake completed and is now open!");
    [webSocket send:@"Hello world!"];
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"The websocket received a message: %@", message);
}
- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}

@end
