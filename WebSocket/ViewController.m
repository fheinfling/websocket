//
//  ViewController.m
//  WebSocket
//
//  Created by Franz Heinfling on 22.09.14.
//  Copyright (c) 2014 Franz Heinfling. All rights reserved.
//

#import "ViewController.h"
#import "SocketRocketWS.h"
#import "JetfireWS.h"
#import "PocketSocketWS.h"

@interface ViewController ()
@property (nonatomic, strong) SocketRocketWS *socketRocketWS;
@property (nonatomic, strong) JetfireWS *jetfireWS;
@property (nonatomic, strong) PocketSocketWS *pocketSocketWS;
@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _socketRocketWS = [[SocketRocketWS alloc] init];
//    [_socketRocketWS connectWebSocket];
//    _jetfireWS = [[JetfireWS alloc] init];
//    [_jetfireWS connectWebSocket];
    _pocketSocketWS = [[PocketSocketWS alloc] init];
    [_pocketSocketWS connectWebSocket];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
